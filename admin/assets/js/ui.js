$(function() {
    //1226 달력 설정 jquery ui
    if($("#datepicker1").size() >=1){
	      $("#datepicker1, #datepicker2").datepicker({
	        dateFormat: 'yy-mm-dd',
	        prevText: '이전 달',
	        nextText: '다음 달',
	        monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	        monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	        dayNames: ['일','월','화','수','목','금','토'],
	        dayNamesShort: ['일','월','화','수','목','금','토'],
	        dayNamesMin: ['일','월','화','수','목','금','토'],
	        // showOn: "button",
            button: false,
	        // buttonImage: "assets/images/calendar.png",
	        buttonImageOnly: false,
	        // buttonText: "날짜선택",
	        showMonthAfterYear: true,
	        changeMonth: true,
	        changeYear: true,
	        onSelect: function( selectedDate ) {
	        	if(this.id == 'datepicker1'){
	        		var dateMin = $('#datepicker1').datepicker("getDate");
	                var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1);
	                $('#datepicker2').datepicker("option","minDate",rMin);
	             }
	        }
	      });
    }
    if($("#datepicker3").size() >=1){
	      $("#datepicker3, #datepicker4").datepicker({
		        dateFormat: 'yy-mm-dd',
		        prevText: '이전 달',
		        nextText: '다음 달',
		        monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		        monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		        dayNames: ['일','월','화','수','목','금','토'],
		        dayNamesShort: ['일','월','화','수','목','금','토'],
		        dayNamesMin: ['일','월','화','수','목','금','토'],
		        showOn: "button",
		        buttonImage: "/resources/images/calendar.png",
		        buttonImageOnly: true,
		        buttonText: "날짜선택",
		        showMonthAfterYear: true,
		        changeMonth: true,
		        changeYear: true,
		        onSelect: function( selectedDate ) {
		        	if(this.id == 'datepicker3'){
		        		var dateMin = $('#datepicker3').datepicker("getDate");
		                var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1);
		                $('#datepicker4').datepicker("option","minDate",rMin);
		             }
		        }
		      });
	    }
    includeLayout();
  });
  function includeLayout(){
    if(location.href.indexOf('html') < 0) return false;
    var includeArea = $('[data-include]');
    var self, url;
    $.each(includeArea, function() {
      self = $(this);
      url = self.data("include");
      self.load(url, function() {
        self.removeAttr("data-include");
     });
    });
  }
 function gnbSetup(a,b){
   $('header ul li').eq(a).find('a').addClass('current');
   $('side ul li').eq(b).find('a').addClass('current');
 }
 //달력 날짜 셋팅
 function setForm(obj){
    $("#datepicker2").datepicker('setDate',new Date);
    $("#datepicker4").datepicker('setDate',new Date);
    var d = new Date();
    //console.log(obj)
    if(obj == undefined){
      d.setDate( d.getDate()- 7)

    }else if(obj == 'today'){
      d.setDate(d)
    }else{
      var lastDayofLastMonth = ( new Date( d.getYear(), d.getMonth(), 0) ).getDate();
      if(d.getDate() > lastDayofLastMonth) {
        d.setDate(lastDayofLastMonth);
      }
      d.setMonth(d.getMonth() - obj);

    }
    $("#datepicker1").datepicker('setDate', d)
    $("#datepicker3").datepicker('setDate', d)
 }

//팝업 닫기
 $(document).on('click','.e_popup_close',function(){
     $('#mask').remove();
 });
//팝업 닫기
 $(document).on('click','.popup_close',function(){
    $('#mask').remove();
 });
