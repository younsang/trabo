$(document).ready(function(){
    userAgentCheck();
    tabUi();
    // lnbUi();
});
function lnbUi(){
    $('.lnb .lnb-dep2 > li > a').each(function(){
        if($(this).next('.lnb-dep3').length){
            $(this).parent().addClass('more');
            $(this).on('click',function(){

                $(this).parent().toggleClass('on').siblings().removeClass('on');
            });
        }
    });
}
function tabUi(){
    var tabTit = $('.txt-list-tab'),
        tabBtn = tabTit.find('li');

    var tabCnt = $('.tab-content'),
        tabIdx = tabCnt.index();

    // load style settings
    tabCnt.not(':eq('+tabIdx+')').hide();
    tabTit.each(function(){
        var defaultTit = $(this).children('li').eq(0);
        defaultTit.addClass('on');
    });
    $('.tab-component').each(function () {
        var defaultCnt = $(this).children('.tab-content').eq(0);
        defaultCnt.addClass('on').show();
    });


    tabBtn.on('click', function(e){
        if($(this).attr('rel')){
            e.preventDefault();

            var $this = $(this),
                thisRel = $this.attr('rel');
                thisClass = $('.'+ thisRel);
                thisText = $this.text();
                target = thisClass.parent('.tab-component').attr('id');

            // content connect
            $('#' + target +  '>.tab-content').hide().removeClass('on');
            $('#' + target + ' .' + thisRel).show().addClass('on');

            // title styling and attr status
            $this.addClass('on').siblings().removeClass('on');
            thisClass.addClass('on').siblings().removeClass('on');
            $this.find('a').attr('title', thisText + 'tab active').parent().siblings().find('a').attr('title','');
        }
    });
}
function userAgentCheck(){
    var ua = window.navigator.userAgent;
    var other = 999;
    var msie = ua.indexOf('MSIE ');

    if(ua.indexOf('Mobile') != -1){
        $('html').addClass('mobile');
    }

    if(ua.toLowerCase().indexOf('safari') != -1){
        if(ua.toLowerCase().indexOf('chrome') != -1){
            $('html').addClass('chrome');
        } else {
            $('html').addClass('safari');
        }
    } else if(ua.toLowerCase().indexOf('firefox') != -1){
        $('html').addClass('firefox');
    } else if(ua.toLowerCase().indexOf('msie 9.0') != -1){
        $('html').addClass('ie ie9');
    } else if(ua.toLowerCase().indexOf('msie 10.0') != -1){
        $('html').addClass('ie ie10');
    } else if(ua.toLowerCase().indexOf('rv:11.0') != -1){
        $('html').addClass('ie ie11');
    }

    if( ua.toLowerCase().indexOf('os x') != -1 ){
        $('html').addClass('ios');
    } else if( ua.toLowerCase().indexOf('Android') != -1 ){
        $('html').addClass('android');
    }
}

// Dim
function createDim(){
    if (!$('.dim').length) {
        $('body').append('<div class="dim"></div>');
    }
    $('.dim').fadeIn(250);
}
function removeDim(){
    $('.dim').fadeOut(250);
}
