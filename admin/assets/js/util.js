/**
 * JAVASCRIPT UTIL FUNCTION 정의
 * 2018. 07. 31., ybh27ann@pocketmobile.co.kr 
 */
/**
 * 데이터 공백여부 체크하여 반환
 * @param obj 검사할 오브젝트 객체
 * @returns true:데이터없음
 */
function isEmpty(obj) {
	if( obj == null ) {
		return true;
	}
	if( obj.trim() == "" ) {
		return true;
	}
	return false;
}
/**
 * 이메일 패턴 검사
 * @param mail 검사할 메일 문자열
 * @returns true:이메일맞음
 */
function isEmail(mail) {
	if(!/^[\w_-]+(\.[\w_-]+)*@[\w_-]+(\.[\w_-]+)*\.\w{2,3}$/.test(mail)) {
		return false;
	}
	return true;
}
/**
 * 숫자인지 여부 검사
 * @param num 검사할 문자열
 * @returns true:숫자
 */
function isNumeric(num) {
	var numLen = num.length;
	for( var i = 0 ; i < numLen ; i++ ) {
		var chr = num.charAt(i);
		if( '0' > chr || '9' < chr ) {
			return false;
		}
	}
	return true;
}
/**
 * 사업자 등록번호를 검사한다.
 * @param 사업자 등록번호
 * @return true:유효한 등록번호
 */
function checkBizSocialNumber(strNo) {
	var sum = 0;
	var getlist = new Array(10);
	var chkvalue = new Array("1","3","7","1","3","7","1","3","5");

	for (var i=0;i<10;i++){
		getlist[i] = strNo.substring(i,i+1);
	}

	for (var i = 0; i < 9; i++){
		sum += getlist[i]*chkvalue[i];
	}
	sum = sum + parseInt((getlist[8] * 5) / 10) ;
	sidliy = sum % 10;
	sidchk = 0;

	if ( sidliy != 0 ) {
		sidchk = 10 - sidliy;
	} else {
		sidchk = 0;
	}
	if ( sidchk != getlist[9] ) {
		return false;
	}
	return true;
}
/**
 * Ajax response status check and return body
 * @param response 응답 데이터 전체 데이터
 * @returns response body data
 */
function ajaxResponse(res) {
	// 데이터 처리 성공
	var success = '0000';
	// 데이터 처리 실패
	var fail = '0001';
	// http 응답 코드
	var code = res.status.code;
	// 성공여부
	if(code == success) {
		return true;
	} else {
		if(code != fail) {
			// http status 
			var status = res.status.status;
			// 예외처리 부가정보
			var moreInfo = status.moreInfo;
			// 의도된 실패 코드가 아닐 경우 에러로 판단하여 페이지 이동 처리한다.
			window.location = '/error/error?errorCode='+status+'&errorMessage='+moreInfo;
		}
		return false;
	}
}
/**
 * 쿠키정보 조회
 * @param id 추출할 쿠키 id
 * @returns 쿠키값
 */
function getCookie(id) {
	// userid 쿠키에서 id 값을 가져온다.
	var cook = document.cookie + ";";
	var idx = cook.indexOf(id, 0);
	var val = "";
	if(idx != -1){
		cook = cook.substring(idx, cook.length);
		begin = cook.indexOf("=", 0) + 1;
		end = cook.indexOf(";", begin);
		val = unescape( cook.substring(begin, end) );
	}
	return val;
}
/**
 * 쿠키 정보 업데이트
 * @param id 쿠키 저장 id
 * @param value 쿠키 저장값
 * @param expiredays 저장 유효기간
 */
function updateCookie(id, value, expiredays) {
	var today = new Date();
	today.setDate(today.getDate() + parseInt(expiredays));
	document.cookie = id + "=" + escape( value ) + "; path=/; expires=" + today.toGMTString() + ";";
}
/**
 * 쿠키 정보 삭제
 * @param id 쿠키 삭제 id
 */
function removeCookie(id) {
	var today = new Date();
	today.setDate(today.getDate() -1);
	document.cookie = id + "=; path=/; expires=" + today.toGMTString() + ";";
}
/**
 * 문자열 입력 시 최대 글자수 제한
 * @param filed 제어할 필드
 * @param maxCount 최대 글자수
 */
function inputlangthChecker(filed, maxCount) {
	$(filed).on('keyup', function() {
        if($(this).val().length > maxCount) {
            $(this).val($(this).val().substring(0, maxCount));
        }
    });
}
/**
 * 날짜 yyyy-MM-dd 포맷으로 반환
 * @param Data() 객체
 * @return yyyy-MM-dd 포맷 날짜 객체
 */
function getDefaultDateFormat(date) {
	var year = date.getFullYear();		    	//yyyy 년도
	var month = (1 + date.getMonth());			//월
	month = month >= 10 ? month : '0' + month;  //월 두자리로 저장
	var day = date.getDate();					//일
	day = day >= 10 ? day : '0' + day;			//일 두자리로 저장
	return year + '-' + month + '-' + day;
}
/**
 * TextArea 글자 갯수제한
 * onkeyup="length_count(this, 80)" onchange="length_count(this, 80)" onfocus="length_count(this, 80)"
 * @param filed Text 필드
 * @param max_count 글자수
 */
function length_count(filed, max_count){
	var str;
	var str_count = 0;
	var cut_count = 0;
	var str_length = filed.value.length;

	for(k=0; k < str_length; k++){
		str = filed.value.charAt(k);
		if(escape(str).length > 4){
			str_count += 2;
		}else{
			// (\r\n은 1byte 처리)
			if(escape(str) != '%0A') {
				str_count++;
			}
		}
		if(max_count < str_count){
			alert("글자수가 "+ max_count +" byte 이상은 사용불가능합니다");
			if(escape(str).length > 4){
				str_count -= 2;
			}else{
				str_count--;
			}
			filed.value = filed.value.substring(0,k);
			break;
		}
	}
}
/**
 * 일정 입력시 형식 제한
 * @param obj 포맷할 값
 * @param format 날짜 사이에 들어갈 값 => .이면 DD.MM.YYYY 이런형식
 * @returns 포맷한 일정 
 */
function date_format(obj, format){
	if(!obj.value) return "";
	 	//obj.value = "" + obj.value.replace(/-/gi,'');
	//공백제거
    obj.value=obj.value.replace(/\s/gi, '');
	if(obj.value.length == 7){  
		obj.value = obj.value.replace(/\-/gi,''); 
		obj.value = obj.value.replace(/(\d{4})(\d{2})(\d{1})/, '$1'+format+'$2'+format+'$3'); 
    }
}
/**
 * input 입력되고 있는 값 -> 천 단위마다 콤마(,) 추가
 * @param obj 제어할 필드
 * @returns 콤마가 찍힌 숫자
 */
function addComma(obj){
	//모든 전역에서 검사했을 때 콤마가 있다면 제거
	var str = "" + obj.value.replace(/,/gi,''); 
	var regx = new RegExp(/(-?\d+)(\d{3})/);
	//0번째 부터 봤을 때 찾으려는 문자열(.)이 있는지 검사
	var bExists = str.indexOf(".",0);  
	//.을 기준으로 분할
	var strArr = str.split('.');  
	//.을 기준으로 소수점 제외 앞의 상수부분이 있다면(.test는 문자열에 일치하는 패턴 있을 때)
	while(regx.test(strArr[0])){  
		//해당 문자에 콤마 삽입
		strArr[0] = strArr[0].replace(regx,"$1,$2"); 
	}  
	//.이 있는 경우
	if (bExists > -1)  {
		obj.value = strArr[0] + "." + strArr[1];  
	} else  {
		obj.value = strArr[0]; 
	}
}
/**
 * 이미 입력된 값 -> 천의 단위에 콤마 추가 -> 소수점은 8자리까지(반올림처리)
 * @param val 제어할 필드의 값
 * @returns 콤마가 찍힌 숫자
 */
function comma(val){
	if(val != null || val.length != 0){
		//콤마 제거하기
		val = val.replace(/,/g, '');
	   	//0번째 부터 봤을 때 찾으려는 문자열(.)이 있는지 검사
	    var bExists = val.indexOf(".",0);  
	    //.을 기준으로 분할
	    var strArr = val.split('.');
	    var decimal = null;
		//3개마다 콤마 넣기
		val = strArr[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g,',');
		//소수점이 있는 경우
		if (bExists > -1)  {
			//소수점 자리수가 8자리 이상일 때
			if(strArr[1].length > 8){
				//9번째 자리가 5 이상이라면
				if(Number(strArr[1].slice(8,9)) >= 5){
					//8번째 자리에 1 더해서 반올림처리
					decimal = "" + (parseInt(strArr[1].slice(7,8)) + 1);
				}else{
				//9번째 자리가 5 미만이라면 그대로입력
					decimal = strArr[1].slice(7,8);
				}
				strArr[1] = strArr[1].slice(0,7);
				return val + "." + strArr[1] + decimal;
			}else{				
				return val + "." + strArr[1];
			}
	    } else  {
	    //소수점 없을 때
	    	return val;
	    }
	}
}